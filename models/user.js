class User {

	#username;
	#socket;
	lastActive;

	constructor(username, socket){
		this.#username = username;
		this.#socket = socket;
		this.lastActive = 0;
	}

	getUsername(){
		return this.#username;
	}

	getSocket(){
		return this.#socket;
	}

}

module.exports = User;