let socket = io();
let intViewportHeight = window.innerHeight;
document.body.style.height = intViewportHeight + "px";

let openMenu = false;
document.getElementById("msg-container").addEventListener('click', clickBody);

const theirMessageClassList = ["py-1", "px-4", "my-1", "ml-4", "mr-12",  "max-w-lg", "rounded-t-lg", "rounded-r-lg", "self-start", "inline-block", "border", "border-purple-500", "bg-purple-100"];
const myMessageClassList = ["py-1", "px-4", "my-1", "mr-4","ml-12", "max-w-lg", "rounded-t-lg", "rounded-l-lg", "self-end", "text-white", "inline-block", "border", "border-purple-600", "bg-purple-600"];

let username = Math.random().toString(36).substring(2, 15);
socket.on('MESSAGE', (message)=> {
	console.log('sending message');
	console.log(message);
	if(message.sender == username)
		displayMyMessage(message);
	else displayTheirMessage(message);
});

socket.emit("USER_CREATED", username);


socket.on('LOAD_MESSAGE_HISTORY', (history) => {
	messages = JSON.parse(history);
	console.log(messages);
	for(let i = 0; i < messages.length; i++){
		if(messages[i].sender == username)
			displayMyMessage(messages[i]);
		else displayTheirMessage(messages[i]);
	}
});

socket.on('CLEAR_MESSAGES', (message) => {
	let msgContainer = document.getElementById("msg-container");
	msgContainer.innerHTML = "";
	
});

document.getElementById('msg-input').onkeypress = function(e){
	if (!e) e = window.event;
	var keyCode = e.keyCode || e.which;
	if (keyCode == '13'){
	  // Enter pressed
	  sendMessage();
	  return false;
	}
}

let uploadInput = document.getElementById('upload-input');
let uploadButton = document.getElementById('upload-button');
uploadInput.addEventListener('change', (e)=>{
	console.log(uploadInput.files);
	const reader = new FileReader();
	reader.onload = function (){
		const img = new Image();
		console.log(reader.result);
		img.src = reader.result;
		img.classList.add('rounded-lg');
		img.classList.add('h-64');
		img.classList.add('max-w-lg');
		img.classList.add('m-2');
		let preview = document.getElementById('image-preview');
		preview.innerHTML = "";
		preview.appendChild(img);
		preview.classList.add('bg-gray-200');
		preview.hidden = 'visible';
	}
	reader.readAsDataURL(uploadInput.files[0]);
});

uploadButton.addEventListener('click', (e) => {
	uploadInput.click();
})


// const chunksQueue = new Array(chunksQuantity).fill().map((_, index) => index).reverse();
// console.log(chunksQueue);

function displayMyMessage(message){
	let myMessage = document.createElement('p');
	myMessage.classList.add(...myMessageClassList);
	let myMessageText = document.createTextNode(message.fullText);
	myMessage.appendChild(myMessageText);
	let messageContainer = document.getElementById('msg-container');
	messageContainer.appendChild(myMessage);
	let objDiv = document.getElementById("msg-container");
	objDiv.scrollTop = objDiv.scrollHeight;
}

function displayTheirMessage(message){
	let theirMessage = document.createElement('div');
	theirMessage.classList.add(...theirMessageClassList);

	let theirMessageSender = document.createElement('p');
	theirMessageSender.classList.add("text-xs");
	theirMessageSender.classList.add("text-gray-600");
	theirMessageSender.appendChild(document.createTextNode("@" + message.sender));
	theirMessage.appendChild(theirMessageSender);
	let theirMessageText = document.createTextNode(message.fullText);
	theirMessage.appendChild(theirMessageText);
	let messageContainer = document.getElementById('msg-container');
	messageContainer.appendChild(theirMessage);
	let objDiv = document.getElementById("msg-container");
	objDiv.scrollTop = objDiv.scrollHeight;
}

function sendMessage(){
	messageInput = document.getElementById('msg-input');
	if(messageInput.value.replace(/\s/g,'') != ""){
		console.log("sending message");
		let message = {};
		message.sender = username;
		message.fullText = messageInput.value;
		socket.emit('MESSAGE', message);
	}
	messageInput.value = "";
}

function clickSettings() {
	let settingsDropdown = document.getElementById("setting-dropdown");

	if(openMenu){
		settingsDropdown.style.visibility = "hidden";
	}else{
		settingsDropdown.style.visibility = "visible";
	}

	openMenu = !openMenu;
}

function clickBody(){
	if(openMenu){
		let settingsDropdown = document.getElementById("setting-dropdown");
		settingsDropdown.style.visibility = "hidden";
		openMenu = !openMenu;
	}
}

function clearMessagesFromServer(){
	socket.emit('CLEAR_MESSAGES', "");
	let msgContainer = document.getElementById("msg-container");
	msgContainer.innerHTML = "";
}



