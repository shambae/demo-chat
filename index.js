
const User = require('./models/user');
// const MinHeap = require('./data_structures/MinHeap.js');
const express = require('express');
const app = require('express')();
const http = require('http').createServer(app);
const io = require('socket.io')(http);
const fork = require('child_process').fork;

let userMap = {};
let messageQueue = [];
let testUser = new User('testing', 213132);
// let child = fork('./data_structures/UsersManager.js');
// child.send({type: 'clean-heap'})

// child.on('message', (res)=>{
// 	switch(res.type){
// 		case 'disconnect-user':
// 			disconnectUser(res.socketId);
// 			break;
// 	}
// })

app.set('PORT', process.env.PORT || 4000)
app.use(express.static('public'));

io.on('connection', function(socket){

	socket.on('USER_CREATED', (username)=>{
		console.log("new user created: " + username);
		let newUser = new User(username, socket);
		userMap[socket.id] = newUser;
		// child.send({type: 'add-user', newUser: newUser, socketId: socket.id})
		// console.log(JSON.stringify(messageQueue));
		socket.emit('LOAD_MESSAGE_HISTORY', JSON.stringify(messageQueue));
		// socket.emit('load-messages', ()=>{ messageQueue });
	});

	socket.on('MESSAGE', function(message){
		console.log('message sent from sender: ' + message.sender + 'fullText : ' + message.fullText);
		// console.log(userMap);
		messageQueue.push(message);
		console.log(JSON.stringify(messageQueue));
		// if(socket.id in userMap){
			io.emit('MESSAGE', message);
		// }
	});
	socket.on('CLEAR_MESSAGES', function(message){
		messageQueue = [];
		console.log("Messages Cleared");
		io.emit('CLEAR_MESSAGES', '');
	});


	socket.on('disconnect', ()=>{
		console.log("user disconnected, deleting user : ");
		delete userMap[socket.id];
	});

});



http.listen(app.get('PORT'),function(){
	console.log('listening on *:'+ app.get('PORT'));
});

function disconnectUser(socketId){
	let socket = userMap[socketId];
	socket.emit('USER_IDLE', "disconnect");
	socket.disconnect();
	socket.close();
}









